
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Actor;
import domain.Message;
import domain.Priority;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class MessageServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private MessageService	messageService;

	@Autowired
	private ActorService	actorService;


	public void sendMessage(final String sender, final String recipient, final String subject, final String body, final Priority priority, final Collection<String> tags, final Class<?> expected) {

		Class<?> caught = null;

		try {
			this.authenticate(sender);

			final Message message = this.messageService.create();
			final Actor senderActor = this.actorService.findByPrincipal();
			final Actor receiverActor = this.actorService.findByUserAccountUsername(recipient);

			message.setSender(senderActor);
			message.setRecipient(receiverActor);
			message.setSubject(subject);
			message.setBody(body);
			message.setPriority(priority);
			message.setTags(tags);

			this.messageService.save(message);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverSendMessage() {

		final Priority priority = new Priority();
		priority.setValue("NEUTRAL");

		final Collection<String> tags = new ArrayList<String>();

		final Object testingData[][] = {
			//			// Enviar mensaje estando autentificado -> true
			//			{
			//				"customer1", "customer2", "subject", "body", priority, tags, null
			//			},
			// Enviar mensaje sin receptor -> false
			{
				"customer1", null, "subject", "body", priority, tags, IllegalArgumentException.class
			},
			// Enviar mensaje sin autentificar -> false
			{
				null, "customer2", "subject", "body", priority, tags, IllegalArgumentException.class
			},
		//			// Enviar mensaje sin asunto -> false
		//			{
		//				"customer1", "customer2", null, "body", priority, tags, IllegalArgumentException.class
		//			},
		//			// Enviar mensaje sin cuerpo -> false
		//			{
		//				"customer1", "customer2", "subject", null, priority, tags, IllegalArgumentException.class
		//			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.sendMessage((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Priority) testingData[i][4], (Collection<String>) testingData[i][5], (Class<?>) testingData[i][6]);
	}

}
