
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Finder;
import domain.HandyWorker;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class FinderServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private FinderService		finderService;

	@Autowired
	private HandyWorkerService	handyWorkerService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para editar un Finder
	 * 
	 * En este caso de uso se permite al handyWorker editar el finder
	 * Para forzar el error pueden darse los siguientes casos:
	 * 
	 * � El usuario no esta autentificado como handyWorker
	 * � Atributos del registro incorrectos
	 */
	public void editFinderTest(final String username, final String keyWord, final Double minPrice, final Double maxPrice, final String category, final String warranty, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.handyWorkerService.checkIfHandyWorker();

			// Obtenemos el finder del usuario
			final HandyWorker hw = this.handyWorkerService.findByPrincipal();

			final Finder f = hw.getFinder();

			f.setKeyWord(keyWord);
			f.setMinPrice(minPrice);
			f.setMaxPrice(maxPrice);
			f.setCategory(category);
			f.setWarranty(warranty);

			// Comprobamos atributos
			this.finderService.checkFinder(f);

			// Guardamos
			this.finderService.save(f);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverEditFinderTest() {

		final Object testingData[][] = {
			// Editar finder sin estar autentificado -> false
			{
				null, "test", 2.0, 15.0, "Category", "Warranty", IllegalArgumentException.class
			},
			// Editar finder autentificado como customer -> false
			{
				"customer1", "test", 2.0, 15.0, "Category", "Warranty", IllegalArgumentException.class
			},
			// Editar finder autentificado como handyWorker -> true
			{
				"handyworker1", "test", 2.0, 15.0, "Category", "Warranty", null
			},
			// Editar finder autentificado como referee -> false
			{
				"referee1", "test", 2.0, 15.0, "Category", "Warranty", IllegalArgumentException.class
			},
			// Editar finder autentificado como admin -> false
			{
				"admin", "test", 2.0, 15.0, "Category", "Warranty", IllegalArgumentException.class
			},
			// Editar finder con atributos v�lidos -> true
			{
				"handyworker1", "test", null, 15.0, "Category", "Warranty", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.editFinderTest((String) testingData[i][0], (String) testingData[i][1], (Double) testingData[i][2], (Double) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}
}
