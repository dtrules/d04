
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Application;
import domain.Status;
import domain.Task;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ApplicationServiceTest extends AbstractTest {

	// Service under test ---------------------------

	@Autowired
	private ApplicationService	applicationService;
	@Autowired
	private HandyWorkerService	handyWorkerService;


	//Tests ------------------------------

	// Caso en que se crea un Application correctamente y guarda datos

	public void createApplication(final String username, final Date moment,
			final Status status, final double price,
			final Collection<String> comments, final Task task,
			final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			// Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final Application application = this.applicationService.create();

			this.applicationService.save(application);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateApplication() {
		final Date moment = new Date(2018, 11, 29, 20, 0);
		final Status status = new Status();
		status.setValue("ACCEPTED");
		
		final Object testingData[][] = {
				// Crear y guardar una application con handyworker autenticado
				{ "handy worker1", moment, status, 15.5,
						Collections.<String> emptySet(), new Task(), null },
				// Crear y guardar una application con un handy worker y con una
				// fecha de publicación posterior a la actual
				{ "handy worker2", moment, status, 25.5,
						Collections.<String> emptySet(), new Task(),
						IllegalArgumentException.class },

		};
		for (int i = 0; i < testingData.length; i++)
			this.createApplication((String) testingData[i][0],
					(Date) testingData[i][1], (Status) testingData[i][2],
					(double) testingData[i][3],
					(Collection<String>) testingData[i][4],
					(Task) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	// Caso en el que borramos una Application

	public void deleteApplication(final String username,
			final int applicationId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			// Buscamos la Aplication
			final Application application = this.applicationService
					.findOne(applicationId);

			// Borramos
			this.applicationService.delete(application);

			// Comprobamos que se ha borrado

			Assert.isNull(this.applicationService.findOne(applicationId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
