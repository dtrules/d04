
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Note;
import domain.Report;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class NoteServiceTest extends AbstractTest {

	@Autowired
	private NoteService			noteService;
	@Autowired
	private ReportService		reportService;
	@Autowired
	private RefereeService		refereeService;
	@Autowired
	private CustomerService		customerService;
	@Autowired
	private HandyWorkerService	handyWorkerService;


	//Caso en que se crea un Task correctamente y guarda datos

	public void createNote(final String username, final Date moment, final Collection<String> customerComments, final Collection<String> refereeComments, final Collection<String> handyWorkerComments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es referee
			this.refereeService.checkIfReferee();
			//Comprobamos si la persona autenticada es referee
			this.customerService.checkIfCustomer();
			//Comprobamos si la persona autenticada es referee
			this.handyWorkerService.checkIfHandyWorker();

			final Note note = this.noteService.create();
			final Report report = this.reportService.create();

			this.noteService.save(note, report);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateReport() {
		final Date moment	= new Date(2018, 10, 1, 10, 0);
		final Date moment1	= new Date(2018, 12, 12, 10, 0);
		
		final Object testingData[][] = {
			// Crear y guardar una note con referee1 autenticado -> true
			{
				"referee1", moment, Collections.<String> emptySet(), Collections.<String> emptySet(), Collections.<String> emptySet(), null
			},
			// Crear y guardar una note con un actor diferente (customer1) -> true
			{
				"customer1", moment, Collections.<String> emptySet(), Collections.<String> emptySet(), Collections.<String> emptySet(), null
			},
			// Crear y guardar una note con un actor diferente (handyWorker1) -> true
			{
				"handyWoker1", moment, Collections.<String> emptySet(), Collections.<String> emptySet(), Collections.<String> emptySet(), null
			},
			// Crear y guardar una task con un sponsor y con una fecha de publicación posterior a la actual -> false
			{
				"sponsor1", moment, Collections.<String> emptySet(), Collections.<String> emptySet(), Collections.<String> emptySet(), IllegalArgumentException.class
			},
			// Crear y guardar una task con un customer y con una fecha de publicación posterior a la actual -> false
			{
				"customer1", moment1, Collections.<String> emptySet(), Collections.<String> emptySet(), Collections.<String> emptySet(), IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createNote((String) testingData[i][0], (Date) testingData[i][1], (Collection<String>) testingData[i][2], (Collection<String>) testingData[i][3], (Collection<String>) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	//Caso en el que borramos una Report

	public void deleteNote(final String username, final int noteId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el task
			final Note note = this.noteService.findOne(noteId);

			//Borramos
			this.noteService.delete(note);

			//Comprobamos que se ha borrado

			Assert.isNull(this.reportService.findOne(noteId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
