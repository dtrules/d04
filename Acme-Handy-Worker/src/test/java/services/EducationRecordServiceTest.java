
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.EducationRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class EducationRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------
	@Autowired
	private EducationRecordService	educationRecordService;
	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un EducationRecord correctamente y guarda datos

	public void createEducationRecord(final String username, final String title, final Date startMoment, final Date endMoment, final String institution, final Collection<String> comments, final String attachment, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final EducationRecord educationRecord = this.educationRecordService.create();

			this.educationRecordService.save(educationRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateEducationRecord() {
		final Date start = new Date(2018, 10, 1, 10, 0);
		final Date end = new Date(2018, 10, 20, 10, 0);

		final Date start1 = new Date(2018, 12, 12, 10, 0);
		final Date end1 = new Date(2018, 12, 20, 10, 0);

		final Date start2 = new Date(2018, 10, 12, 10, 0);
		final Date end2 = new Date(2018, 10, 3, 10, 0);

		final Object testingData[][] = {
			// Crear y guardar una EducationRecord con handy worker autenticado
			{
				"handy worker1", "Esto es un titulo", start, end, "Esto es una institucion", Collections.<String> emptySet(), "Esto es un attachment", null
			},
			// Crear y guardar un EducationRecord con fecha inicio despues de la actual
			{
				"handy worker2", "Esto es un titulo", start1, end1, "Esto es una institucion", Collections.<String> emptySet(), "Esto es un attachment", IllegalArgumentException.class
			},
			// Crear y guardar una EducationRecord con fecha inicio despues de fecha fin
			{
				"handy worker3", "Esto es un titulo", start2, end2, "Esto es una institucion", Collections.<String> emptySet(), "Esto es un attachment", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createEducationRecord((String) testingData[i][0], (String) testingData[i][1], (Date) testingData[i][2], (Date) testingData[i][3], (String) testingData[i][4], (Collection<String>) testingData[i][5], (String) testingData[i][6],
				(Class<?>) testingData[i][7]);
	}

	//Caso en el que borramos un educationRecord

	public void deleteEducationRecord(final String username, final int educationRecordId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el educationRecord
			final EducationRecord educationRecord = this.educationRecordService.findOne(educationRecordId);

			//Borramos
			this.educationRecordService.delete(educationRecord);

			//Comprobamos que se ha borrado

			Assert.isNull(this.educationRecordService.findOne(educationRecordId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
