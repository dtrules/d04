
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import utilities.AbstractTest;
import domain.Referee;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class RefereeServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private RefereeService	refereeService;

	@Autowired
	private ActorService	actorService;


	// Templates --------------------------------------------------------------

	public void listRefereeTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.refereeService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver los sponsorships que ha creado el sponsor logueado
	 * 
	 * 
	 * 
	 * En este caso de uso se listan los sponsorships desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de sponsorships sin autenticarnos.
	 */
	public void listNotesTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			final Referee referee = this.refereeService.findByPrincipal();

			referee.getReports();
			this.refereeService.checkIfReferee();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de sponsor
	 * 
	 * En este caso de uso se llevara a cabo el registro de un sponsor en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � Nombre de usuario ya existente
	 */
	public void registerSponsor(final String username, final String newUsername, final String password, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber,
		final String address, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);
			Assert.isNull(this.actorService.findByPrincipal());

			// Inicializamos los atributos para la creaci�n
			final Referee referee = this.refereeService.create();

			referee.setName(name);
			referee.setMiddleName(middleName);
			referee.setSurname(surname);
			referee.setPhoto(photo);
			referee.setEmail(email);
			referee.setPhoneNumber(phoneNumber);
			referee.setAddress(address);

			final UserAccount ua = referee.getUserAccount();
			ua.setUsername(newUsername);
			ua.setPassword(password);
			referee.setUserAccount(ua);

			// Comprobamos atributos
			this.refereeService.checkReferee(referee);

			// Guardamos
			this.refereeService.save(referee);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListSponsorTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con sponsor -> true
			{
				"referee1", null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listRefereeTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListSponsorshipsTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"referee1", null
			},
			// Se accede con sponsor -> true
			{
				"customer2", null
			},
			// Se accede con admin -> false
			{
				"admin", IllegalArgumentException.class
			},

			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listNotesTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRegisterSponsor() {

		final Object testingData[][] = {
			// Registrar sponsor sin estar autentificado -> true
			{
				null, "referee3", "referee3", "Referee3Name", "referee3middlename", "referee3surname", "www.linkedin.com/testpicture.jpg", "referee3@gmail.com", "612239922", "Abbey Road, 21", null
			},
			// Registrar sponsor sin estar autentificado -> true
			{
				null, "referee4", "referee4", "Referee4Name", "referee4middlename", "referee4surname", "www.linkedin.com/testpicture.jpg", "referee4@gmail.com", "612239922", "Abbey Road, 21", null
			},
			// Registrar sponsor con datos no v�lidos -> false
			{
				null, "referee4", "referee4", null, "referee4middlename", null, "www.linkedin.com/testpicture.jpg", "referee4@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar sponsor con datos no v�lidos -> false
			{
				null, "referee4", "referee4", "Referee4Name", "referee4middlename", "referee4surname", "thisisnotalink", "referee4@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar sponsor con datos no v�lidos -> false
			{
				null, "referee4", "referee4", "Referee4Name", "referee4middlename", "referee4surname", "www.linkedin.com/testpicture.jpg", "thisisnotamail", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar sponsor estando autentificado -> false
			{
				"admin", "referee3", "referee3", "referee3Name", "referee3middlename", "referee3surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar sponsor estando autentificado -> false
			{
				"customer1", "referee3", "referee3", "Referee3Name", "referee3middlename", "referee3surname", "www.linkedin.com/testpicture.jpg", "referee3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerSponsor((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

}
