
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Category;

@Transactional
@ContextConfiguration(locations = {
	"classpath:spring/junit.xml"
})
@RunWith(SpringJUnit4ClassRunner.class)
public class CategoryServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private CategoryService			categoryService;

	@Autowired
	private AdministratorService	administratorService;


	//Tests -----------------------------------------

	//Caso en que se crea una category correctamente y se guarda datos

	public void createCategory(final String username, final String name, final Collection<Category> subCategories, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			this.administratorService.checkIfAdmin();
			final Category category = this.categoryService.create();
			category.setName(name);
			category.setParentCategory(this.categoryService.findCATEGORY());
			category.setSubCategories(subCategories);
			this.categoryService.save(category);
			super.unauthenticate();
		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateCategory() {

		final Collection<Category> categories = new ArrayList<Category>();

		final Object testingData[][] = {
			//Comprobar que un admin es el que puede crear las categorias -> true
			{
				"admin", "Barcelona", categories, null
			},
			//Comprobar que si un actor que no sea admin crea la category FALLA -> false
			{
				"sponsor2", "Barcelona", categories, IllegalArgumentException.class
			},

		//			//Comprobar que un admin puede crear una category y que tiene categoria padre Null --> FALLA
		//			{
		//				"admin", "Barcelona", null, IllegalArgumentException.class
		//
		//			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createCategory((String) testingData[i][0], (String) testingData[i][1], (Collection<Category>) testingData[i][2], (Class<?>) testingData[i][3]);

	}

	//	//Caso en el que borramos una category
	//
	//	public void deleteCategory(final String username, final Class<?> expected) {
	//		Class<?> caught = null;
	//		try {
	//
	//			this.authenticate(username);
	//			//Buscamos la category
	//			final Collection<Category> categories = this.categoryService.findAll();
	//			Category category = (Category) categories.toArray()[0];
	//			final Category categoryParent = this.categoryService.findCATEGORY();
	//
	//			//Borramos
	//			if (category.equals(categoryParent)) {
	//				category = (Category) categories.toArray()[1];
	//				this.categoryService.delete(category);
	//			} else
	//				this.categoryService.delete(category);
	//
	//			//Comprobamos que se ha borrado
	//			//Assert.isNull(this.categoryService.findOne(category.getId()));
	//			this.unauthenticate();
	//		}
	//
	//		catch (final Throwable oops) {
	//
	//			caught = oops.getClass();
	//		}
	//
	//		this.checkExceptions(expected, caught);
	//	}
	// Drivers ----------------------------------------------------------------------

	//	@Test
	//	public void driverDeleteCategory() {
	//
	//		final Object testingData[][] = {
	//			// borrar una category de la base de datos siendo admin
	//			//y que no esta asignada a una task -> true
	//			{
	//				"admin", null
	//			},
	//			// borrar una category de la base de datos siendo un actor diferente -> false
	//			{
	//				"sponsor2", IllegalArgumentException.class
	//			},
	//			//borrar una category mediante un admin y que dicha category
	//			// este referenciada a una task -> false
	//			{
	//				"admin", IllegalArgumentException.class
	//			},
	//
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.deleteCategory((String) testingData[i][0], (Class<?>) testingData[i][1]);
	//	}

}
