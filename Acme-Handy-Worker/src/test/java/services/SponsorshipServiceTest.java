
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.CreditCard;
import domain.Sponsorship;
import domain.Tutorial;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SponsorshipServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private SponsorshipService	sponsorshipService;

	@Autowired
	private TutorialService		tutorialService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para ver todos los Sponsorships
	 * 
	 * En este caso de uso se listan los sponsorships desde su correspondiente vista
	 */
	public void listSponsorshipTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.sponsorshipService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de sponsorship
	 * 
	 * En este caso de uso se llevara a cabo el registro de un sponsorship en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como sponsor
	 * � Atributos del registro incorrectos
	 */
	public void registerSponsorshipTest(final String username, final String banner, final String targetPage, final CreditCard creditCard, final int tutorialId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Inicializamos los atributos para la creaci�n
			final Sponsorship sponsorship = this.sponsorshipService.create();

			sponsorship.setBanner(banner);
			sponsorship.setTargetPage(targetPage);

			sponsorship.setCreditCard(creditCard);

			final Tutorial t = this.tutorialService.findOne(tutorialId);
			sponsorship.setTutorial(t);

			// Comprobamos atributos
			this.sponsorshipService.checkSponsorship(sponsorship);

			// Guardamos
			this.sponsorshipService.save(sponsorship);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Borrado de sponsorship
	 * 
	 * En este caso de uso se llevara a cabo el borrado de un sponsorship en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como sponsor
	 * � El sponsorship no existe
	 */
	public void deleteSponsorshipTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el sponsorship
			final Sponsorship s = (Sponsorship) this.sponsorshipService.findAll().toArray()[0];

			// Borramos
			this.sponsorshipService.delete(s);

			// Comprobamos que se ha borrado
			//Assert.isNull(this.sponsorshipService.findOne(sponsorshipId));

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListSponsorshipTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			//  Se accede con admin -> true
			{
				"admin", null
			},
			//  Se accede con customer -> true
			{
				"customer1", null
			},
			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listSponsorshipTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//	@Test
	//	public void driverRegisterSponsorshipTest() {
	//
	//		final CreditCard creditCard = new CreditCard();
	//
	//		creditCard.setHolderName("HolderNameCreditCard33");
	//		creditCard.setBrandName("VISA");
	//		creditCard.setNumber(4013200209774812L);
	//		creditCard.setExpirationMonth(02);
	//		creditCard.setExpirationYear(2020);
	//		creditCard.setCvvCode(354);
	//
	//		final Object testingData[][] = {
	//			// Registrar sponsorship sin estar autentificado -> false
	//			{
	//				null, "www.banner.com/test1.jpg", "www.targetpage.com", creditCard, 3156, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship autentificado como customer -> false
	//			{
	//				"customer1", "www.banner.com/test1.jpg", "www.targetpage.com", creditCard, 3156, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship autentificado como admin -> false
	//			{
	//				"admin", "www.banner.com/test1.jpg", "www.targetpage.com", creditCard, 3156, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship con datos no v�lidos -> false
	//			{
	//				"sponsor1", "thisisnotabanner", "www.targetpage.com", creditCard, 3156, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship con datos no v�lidos -> false
	//			{
	//				"sponsor1", "www.banner.com/test1.jpg", "thisisnotatargetpage", creditCard, 3156, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship con datos no v�lidos -> false
	//			{
	//				"sponsor1", "www.banner.com/test1.jpg", "www.targetpage.com", null, 3156, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship con datos no v�lidos -> false
	//			{
	//				"sponsor1", "www.banner.com/test1.jpg", "www.targetpage.com", creditCard, 0, IllegalArgumentException.class
	//			},
	//			// Registrar sponsorship autentificado como Sponsor y con datos v�ldios -> true
	//			{
	//				"sponsor1", "www.banner.com/test1.jpg", "www.targetpage.com", creditCard, 3156, null
	//			},
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.registerSponsorshipTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (CreditCard) testingData[i][3], (int) testingData[i][4], (Class<?>) testingData[i][5]);
	//	}

	@Test
	public void driverDeleteSponsorshipTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			//  Se accede con admin -> false
			{
				"admin", IllegalArgumentException.class
			},
			//  Se accede con customer -> false
			{
				"customer1", IllegalArgumentException.class
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteSponsorshipTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}
}
