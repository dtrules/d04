
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Complaint;
import domain.Note;
import domain.Report;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ReportServiceTest extends AbstractTest {

	//Service under test------------------------
	@Autowired
	private ReportService	reportService;

	@Autowired
	private RefereeService	refereeService;


	//Caso en que se crea un Task correctamente y guarda datos

	public void createReport(final String username, final Date moment, final String description, final Collection<String> attachments, final Collection<Note> notes, final Complaint complaint, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es customer
			this.refereeService.checkIfReferee();
			final Report report = this.reportService.create();
			report.setMoment(moment);
			report.setDescription(description);
			report.setAttachments(attachments);
			report.setNotes(notes);
			report.setComplaint(complaint);
			this.reportService.save(report);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateReport() {

		final Object testingData[][] = {
			// Crear y guardar una report con referee1 autenticado -> true
			{
				"referee1", "01/10/2018 10:00", "Description1", Collections.<String> emptySet(), Collections.<Note> emptySet(), 3005, null
			},
			// Crear y guardar una report con un actor diferente (sponsor2) -> false
			{
				"sponsor2", "01/10/2018 10:00", "Description1", Collections.<String> emptySet(), Collections.<Note> emptySet(), 3005, IllegalArgumentException.class
			},
			// Crear y guardar una task con un referee y con una fecha de publicación posterior a la actual -> false
			{
				"referee1", "12/12/2018 21:00", "Description1", Collections.<String> emptySet(), Collections.<Note> emptySet(), 3005, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createReport((String) testingData[i][0], (Date) testingData[i][1], (String) testingData[i][2], (Collection<String>) testingData[i][3], (Collection<Note>) testingData[i][4], (Complaint) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	//Caso en el que borramos una Report

	public void deleteReport(final String username, final int reportId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el task
			final Report report = this.reportService.findOne(reportId);

			//Borramos
			this.reportService.delete(report);

			//Comprobamos que se ha borrado

			Assert.isNull(this.reportService.findOne(reportId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

}
