
package services;

import java.util.Collection;
import java.util.Collections;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Complaint;
import domain.Customer;
import domain.HandyWorkerEndorsement;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class HandyWorkerEndorsementServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private HandyWorkerEndorsementService	handyWorkerEndorsementService;

	@Autowired
	private CustomerService					customerService;

	@Autowired
	private HandyWorkerService				handyWorkerService;


	//Caso en que se crea un CustomerEndorsement correctamente y guarda datos

	public void createHandyWorkerEndorsement(final String username, final double score, final Collection<String> comments, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es customer
			this.handyWorkerService.checkIfHandyWorker();

			final Customer customer = (Customer) this.customerService.findAll().toArray()[0];

			final HandyWorkerEndorsement handyWorkerEndorsement = this.handyWorkerEndorsementService.create();
			handyWorkerEndorsement.setScore(score);
			handyWorkerEndorsement.setCustomer(customer);
			handyWorkerEndorsement.setComments(comments);
			this.handyWorkerEndorsementService.save(handyWorkerEndorsement);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateHandyWorkerEndorsement() {

		final Object testingData[][] = {
			// Crear y guardar un HandyWorkerEndorsement con handyworker autenticado -> true
			{
				"handyworker1", 32.2, Collections.<Complaint> emptySet(), null
			},
			// Crear y guardar un HandyWorkerEndorsement con un actor diferente (sponsor2) -> false
			{
				"sponsor2", 32.2, Collections.<Complaint> emptySet(), IllegalArgumentException.class
			},
			// Crear y guardar un HandyWorkerEndorsement con un customer con unos comentarios nulos -> true
			{
				"handyworker1", 32.2, null, null
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createHandyWorkerEndorsement((String) testingData[i][0], (double) testingData[i][1], (Collection<String>) testingData[i][2], (Class<?>) testingData[i][3]);
	}

	//Caso en el que borramos una HandyWorkerEndorsement

	public void deleteHandyWorkerEndorsement(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el HandyWorkerEndorsement
			final HandyWorkerEndorsement handyWorkerEndorsement = (HandyWorkerEndorsement) this.handyWorkerEndorsementService.findAll().toArray()[0];

			//Borramos
			this.handyWorkerEndorsementService.delete(handyWorkerEndorsement);

			//Comprobamos que se ha borrado

			//Assert.isNull(this.customerEndorsementService.findOne(customerEndorsement.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverDeleteHandyWorkerEndorsement() {

		final Object testingData[][] = {
			// borrar una HandyWorkerEndorsement de la base de datos siendo handyworker -> true
			{
				"handyworker1", null
			},
			// borrar una task de la base de datos siendo un actor diferente -> false
			{
				"sponsor2", IllegalArgumentException.class
			},
			//borrar una task de un customer que no es suya -> false
			{
				"customer1", IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteHandyWorkerEndorsement((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
