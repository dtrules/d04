
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Section;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SectionServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private SectionService	sectionService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para ver todas las sections
	 * 
	 * En este caso de uso se listan las sections desde su correspondiente vista
	 */
	public void listSectionTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.sectionService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de section
	 * 
	 * En este caso de uso se llevara a cabo el registro de una section en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como handyworker
	 * � Atributos del registro incorrectos
	 */
	public void registerSectionTest(final String username, final String title, final String text, final Collection<String> pictures, final int number, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Inicializamos los atributos para la creaci�n
			final Section section = this.sectionService.create();

			section.setTitle(title);
			section.setText(text);
			section.setPictures(pictures);
			section.setNumber(number);

			// Comprobamos atributos
			this.sectionService.checkSection(section);

			// Guardamos
			this.sectionService.save(section);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Borrado de section
	 * 
	 * En este caso de uso se llevara a cabo el borrado de una section en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como handyworker
	 * � La section no existe
	 */
	public void deleteSectionTest(final String username, final int sectionId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos la section
			final Section s = this.sectionService.findOne(sectionId);

			// Borramos
			this.sectionService.delete(s);

			// Comprobamos que se ha borrado
			Assert.isNull(this.sectionService.findOne(sectionId));

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListSectionTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			//  Se accede con admin -> true
			{
				"admin1", null
			},
			//  Se accede con customer -> true
			{
				"customer1", null
			},
			//  Se accede con handyworker -> true
			{
				"handywoker1", null
			},
			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listSectionTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRegisterSectionTest() {

		final Collection<String> pictures = new ArrayList<String>();
		pictures.add("www.picturetest.com/image1.jpg");

		final Object testingData[][] = {
			// Registrar tutorial sin estar autentificado -> false
			{
				null, "title1", "text1", pictures, 1, IllegalArgumentException.class
			},
			// Registrar tutorial autentificado como customer -> false
			{
				"customer1", "title1", "summary1", pictures, 1, IllegalArgumentException.class
			},
			// Registrar tutorial autentificado como sponsor -> false
			{
				"sponsor1", "title1", "summary1", pictures, 1, IllegalArgumentException.class
			},
			// Registrar tutorial con datos no v�lidos -> false
			{
				"handyworker1", "title1", "summary1", pictures, 1, IllegalArgumentException.class
			},
			// Registrar tutorial autentificado como HandyWorker y con datos v�ldios -> true
			{
				"handyworker1", "title1", "summary1", pictures, 1, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerSectionTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<String>) testingData[i][3], (int) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	@Test
	public void driverDeleteSectionTest() {
		final Object testingData[][] = {
			// Se accede con handyworker -> true
			{
				"handyworker1", null, null
			},
			// Se accede con sponsor -> false
			{
				"sponsor2", null, IllegalArgumentException.class
			},
			//  Se accede con admin -> false
			{
				"admin1", null, IllegalArgumentException.class
			},
			//  Se accede con customer -> false
			{
				"customer1", null, IllegalArgumentException.class
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, null, IllegalArgumentException.class
			},
			//  Se borra una id que no existe -> false
			{
				"handyworker1", 1212112, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteSectionTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
