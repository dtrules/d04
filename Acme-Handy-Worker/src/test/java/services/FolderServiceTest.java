
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Folder;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class FolderServiceTest extends AbstractTest {

	// Services and repositories
	@Autowired
	private FolderService	folderService;

	@Autowired
	private ActorService	actorService;


	public void deleteFolder(final String username, final String folder, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			Assert.isTrue(this.actorService.findByPrincipal() != null);

			final Folder f = this.folderService.findFolderByNameAndActorId(this.actorService.findByPrincipal().getId(), folder);

			this.folderService.delete(f);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverDeleteFolder() {

		final Object testingData[][] = {
			// Eliminar carpeta sin estar autentificado -> false
			{
				null, "spambox", IllegalArgumentException.class
			},
			// Eliminar carpeta del sistema -> false
			{
				"customer1", "spambox", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteFolder((String) testingData[i][0], (String) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
