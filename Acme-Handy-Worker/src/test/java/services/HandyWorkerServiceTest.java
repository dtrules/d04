
package services;

import java.util.Collection;
import java.util.Collections;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import utilities.AbstractTest;
import domain.Application;
import domain.Curriculum;
import domain.Finder;
import domain.HandyWorker;
import domain.HandyWorkerEndorsement;
import domain.Tutorial;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class HandyWorkerServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private HandyWorkerService	handyWorkerService;


	//Tests -----------------------------------------

	//Caso en que se crea un Application correctamente y guarda datos

	public void createHandyWorker(final String username, final String make, final Collection<Application> applications, final Collection<Tutorial> tutorials, final Curriculum curriculum, final Finder finder,
		final Collection<HandyWorkerEndorsement> handyWorkerEndorsements, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final HandyWorker handyWorker = this.handyWorkerService.create();

			this.handyWorkerService.save(handyWorker);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateHandyWorker() {

		final Object testingData[][] = {
			// Crear y guardar un handy worker con actor autenticado
			{
				"actor 1", "Tom�s Jim�nez Ponce", Collections.<Application> emptySet(), Collections.<Tutorial> emptySet(), new Curriculum(), new Finder(), Collections.<HandyWorkerEndorsement> emptySet(), null
			},
			// Crear y guardar un handy worker y con make vacio
			{
				"actor 2", "", Collections.<Application> emptySet(), Collections.<Tutorial> emptySet(), new Curriculum(), new Finder(), Collections.<HandyWorkerEndorsement> emptySet(), IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createHandyWorker((String) testingData[i][0], (String) testingData[i][1], (Collection<Application>) testingData[i][2], (Collection<Tutorial>) testingData[i][3], (Curriculum) testingData[i][4], (Finder) testingData[i][5],
				(Collection<HandyWorkerEndorsement>) testingData[i][6], (Class<?>) testingData[i][7]);
	}

	// Caso en el que borramos un HandyWorker

	public void deleteHandyWorker(final String username, final int handyWorkerId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			// Buscamos al handy worker
			final HandyWorker handyWorker = (HandyWorker) this.handyWorkerService.findAll().toArray()[0];

			// Borramos
			this.handyWorkerService.delete(handyWorker);

			// Comprobamos que se ha borrado

			//Assert.isNull(this.handyWorkerService.findOne(handyWorkerId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
