
package services;

import java.util.Date;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Phase;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class PhaseServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private PhaseService		phaseService;

	@Autowired
	private HandyWorkerService	handyWorkerService;

	@Autowired
	private TaskService			taskService;


	//Tests -----------------------------------------

	public void createPhase(final String username, final String title, final String description, final Date startMoment, final Date endMoment, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);
			this.handyWorkerService.checkIfHandyWorker();
			final Phase phase = this.phaseService.create();
			//			final Task task = (Task) this.taskService.findAll().toArray()[0];
			phase.setTitle(title);
			phase.setDescription(description);
			phase.setStartMoment(startMoment);
			phase.setEndMoment(endMoment);
			this.phaseService.save(phase);
			super.unauthenticate();
		}

		catch (final Throwable oops) {
			caught = oops.getClass();
		}
		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreatePhase() {
		final Date testStart = new Date(2018, 12, 2, 10, 0);

		final Date testEnd = new Date(2020, 12, 7, 10, 0);

		final Date testStart1 = new Date(2019, 12, 2, 10, 0);
		final Date testEnd1 = new Date(2021, 12, 7, 10, 0);

		final Object testingData[][] = {
			//Comprobar que un HandyWorker puede crear una Phase --> true

			{
				"handyWorker1", "Phase21", "Description of Phase1", testStart, testEnd, null
			},
			//Comprobar que un actor distinto de HandyWorker no puede crear una Phase --> false
			{
				"customer1", "Phase21", "Description of Phase1", testStart, testEnd, IllegalArgumentException.class
			},

			//La phase no puede tener fechas fueras del periodo de la task --> false
			{
				"handyWorker1", "Phase21", "Description of Phase1", testStart1, testEnd1, IllegalArgumentException.class

			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createPhase((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Date) testingData[i][3], (Date) testingData[i][4], (Class<?>) testingData[i][5]);

	}

	//Caso en el que borramos una category

	public void deletePhase(final String username, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);
			//Buscamos la phase
			final Phase phase = (Phase) this.phaseService.findAll().toArray()[0];

			//Borramos
			this.phaseService.delete(phase);

			//Comprobamos que se ha borrado
			Assert.isNull(this.phaseService.findOne(phase.getId()));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverDeletePhase() {

		final Object testingData[][] = {
			// borrar una phase de la base de datos siendo handyWorker -> true
			{
				"handyWorker1", null
			},
			// borrar una phase de la base de datos siendo un actor diferente -> false
			{
				"sponsor2", IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deletePhase((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

}
