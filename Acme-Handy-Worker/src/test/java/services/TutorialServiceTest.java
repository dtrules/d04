
package services;

import java.util.ArrayList;
import java.util.Collection;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Section;
import domain.Tutorial;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class TutorialServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private TutorialService	tutorialService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para ver todos los tutorials
	 * 
	 * En este caso de uso se listan los tutorials desde su correspondiente vista
	 */
	public void listTutorialTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.tutorialService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de tutorial
	 * 
	 * En este caso de uso se llevara a cabo el registro de un tutorial en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como handyworker
	 * � Atributos del registro incorrectos
	 */
	public void registerTutorialTest(final String username, final String title, final String summary, final Collection<String> pictures, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Inicializamos los atributos para la creaci�n
			final Tutorial tutorial = this.tutorialService.create();

			tutorial.setTitle(title);
			tutorial.setSummary(summary);
			tutorial.setPictures(pictures);

			final Section s = new Section();
			s.setNumber(1);
			s.setPictures(pictures);
			s.setText("SectionTest");
			s.setTitle("SectionTest");
			final Collection<Section> sections = new ArrayList<Section>();
			sections.add(s);

			tutorial.setSections(sections);

			// Comprobamos atributos
			this.tutorialService.checkTutorial(tutorial);

			// Guardamos
			this.tutorialService.save(tutorial);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Borrado de tutorial
	 * 
	 * En este caso de uso se llevara a cabo el borrado de un tutorial en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como handyworker
	 * � El tutorial no existe
	 */
	public void deleteTutorialTest(final String username, final int tutorialId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el sponsorship
			final Tutorial t = this.tutorialService.findOne(tutorialId);

			// Borramos
			this.tutorialService.delete(t);

			// Comprobamos que se ha borrado
			Assert.isNull(this.tutorialService.findOne(tutorialId));

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListTutorialTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			//  Se accede con admin -> true
			{
				"admin1", null
			},
			//  Se accede con customer -> true
			{
				"customer1", null
			},
			//  Se accede con handyworker -> true
			{
				"handywoker1", null
			},
			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listTutorialTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRegisterTutorialTest() {

		final Collection<String> pictures = new ArrayList<String>();
		pictures.add("www.picturetest.com/image1.jpg");

		final Object testingData[][] = {
			// Registrar tutorial sin estar autentificado -> false
			{
				null, "title1", "summary1", pictures, IllegalArgumentException.class
			},
			// Registrar tutorial autentificado como customer -> false
			{
				"customer1", "title1", "summary1", pictures, IllegalArgumentException.class
			},
			// Registrar tutorial autentificado como sponsor -> false
			{
				"sponsor1", "title1", "summary1", pictures, IllegalArgumentException.class
			},
			// Registrar tutorial con datos no v�lidos -> false
			{
				"handyworker1", "title1", "summary1", pictures, IllegalArgumentException.class
			},
			// Registrar tutorial autentificado como HandyWorker y con datos v�ldios -> true
			{
				"handyworker1", "title1", "summary1", pictures, null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerTutorialTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (Collection<String>) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverDeleteTutorialTest() {
		final Object testingData[][] = {
			// Se accede con handyworker -> true
			{
				"handyworker1", 3156, null
			},
			// Se accede con sponsor -> false
			{
				"sponsor2", 3156, IllegalArgumentException.class
			},
			//  Se accede con admin -> false
			{
				"admin1", 3156, IllegalArgumentException.class
			},
			//  Se accede con customer -> false
			{
				"customer1", 3156, IllegalArgumentException.class
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, 3156, IllegalArgumentException.class
			},
			//  Se borra una id que no existe -> false
			{
				"handyworker1", 1212112, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteTutorialTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
