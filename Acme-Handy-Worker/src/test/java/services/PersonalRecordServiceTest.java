
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.PersonalRecord;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class PersonalRecordServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private PersonalRecordService	personalRecordService;
	@Autowired
	private HandyWorkerService		handyWorkerService;


	// Test ------------------------

	//Caso en que se crea un PersonalRecord correctamente y guarda datos

	public void createPersonalRecord(final String username, final String name, final String photo, final String email, final String phoneNumber, final String linkedInProfile, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es handy worker
			this.handyWorkerService.checkIfHandyWorker();

			final PersonalRecord personalRecord = this.personalRecordService.create();

			this.personalRecordService.save(personalRecord);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreatePersonalRecord() {

		final Object testingData[][] = {
			// Crear y guardar un PersonalRecord con handy worker autenticado
			{
				"handy worker1", "Tom�s", "Esto es una foto", "tomasjimenezponce@hotmail.com", "685574490", "Esto es un linkedIn Profile", null
			},
			// Crear y guardar un PersonalRecord con 10 digitos en numero de telefono
			{
				"handy worker2", "Tom�s", "Esto es una foto", "tomasjimenezponce@hotmail.com", "6855749490", "Esto es un linkedIn Profile", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createPersonalRecord((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (Class<?>) testingData[i][6]);
	}

	//Caso en el que borramos un personalRecord

	public void deletePersonalRecord(final String username, final int personalRecordId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el educationRecord
			final PersonalRecord personalRecord = this.personalRecordService.findOne(personalRecordId);

			//Borramos
			this.personalRecordService.delete(personalRecord);

			//Comprobamos que se ha borrado

			Assert.isNull(this.personalRecordService.findOne(personalRecordId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}
}
