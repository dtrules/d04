
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Warranty;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class WarrantyServiceTest extends AbstractTest {

	//Service under test------------------------
	@Autowired
	private WarrantyService			warrantyService;

	@Autowired
	private AdministratorService	administratorService;


	//Caso en que se crea un Task correctamente y guarda datos

	public void createWarranty(final String username, final String title, final String laws, final String terms, final boolean draftMode, final Class<?> expected) {
		Class<?> caught = null;

		try {
			super.authenticate(username);

			//Comprobamos si la persona autenticada es customer
			this.administratorService.checkIfAdmin();
			final Warranty warranty = this.warrantyService.create();
			warranty.setTitle(title);
			warranty.setLaws(laws);
			warranty.setTerms(terms);
			warranty.setDraftMode(draftMode);
			this.warrantyService.save(warranty);

			super.unauthenticate();

		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverCreateWarranty() {

		final Object testingData[][] = {
			// Crear y guardar una warranty con administrator1 autenticado -> true
			{
				"administrator1", "Title1", "Laws1", "Terms1", true, null
			},
			// Crear y guardar una warranty con un actor diferente (sponsor1) -> false
			{
				"sponsor2", "Title1", "Laws1", "Terms1", true, IllegalArgumentException.class
			},
			// Crear y guardar una warranty con draftmode false -> false
			{
				"administrator1", "Title1", "Laws1", "Terms1", false, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.createWarranty((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (boolean) testingData[i][4], (Class<?>) testingData[i][5]);
	}

	//Caso en el que borramos una Task

	public void deleteWarranty(final String username, final int warrantyId, final Class<?> expected) {
		Class<?> caught = null;
		try {

			this.authenticate(username);

			//Buscamos el warranty
			final Warranty warranty = this.warrantyService.findOne(warrantyId);

			//Borramos
			this.warrantyService.delete(warranty);

			//Comprobamos que se ha borrado

			Assert.isNull(this.warrantyService.findOne(warrantyId));
			this.unauthenticate();
		}

		catch (final Throwable oops) {

			caught = oops.getClass();
		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverDeleteWarranty() {

		final Object testingData[][] = {
			// borrar una warranty de la base de datos siendo administrator -> true
			{
				"administrator1", 3157, null
			},
			// borrar una warranty de la base de datos siendo un actor diferente -> false
			{
				"sponsor2", 3157, IllegalArgumentException.class
			},
			//borrar una warranty de un administrator que no es suya -> false
			{
				"administrator2", 3157, IllegalArgumentException.class
			},

		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteWarranty((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}

}
