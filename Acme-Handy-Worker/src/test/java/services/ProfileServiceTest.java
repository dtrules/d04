
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import utilities.AbstractTest;
import domain.Profile;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class ProfileServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private ProfileService	profileService;


	// Templates --------------------------------------------------------------

	/*
	 * Vista para ver todos los Profiles
	 * 
	 * En este caso de uso se listan los profiles desde su correspondiente vista
	 * � El usuario no esta autentificado
	 */
	public void listProfileTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.profileService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de profile
	 * 
	 * En este caso de uso se llevara a cabo el registro de un profile en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado
	 * � Atributos del registro incorrectos
	 */
	public void registerProfileTest(final String username, final String nick, final String socialNetwork, final String link, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Inicializamos los atributos para la creaci�n
			final Profile profile = this.profileService.create();

			profile.setNick(nick);
			profile.setSocialNetwork(socialNetwork);
			profile.setLink(link);

			// Comprobamos atributos
			this.profileService.checkProfile(profile);

			// Guardamos
			this.profileService.save(profile);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Borrado de profile
	 * 
	 * En este caso de uso se llevara a cabo el borrado de un profile en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario no esta autentificado como profile
	 * � El profile no existe
	 */
	public void deleteSponsorshipTest(final String username, final int profileId, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Buscamos el profile
			final Profile p = this.profileService.findOne(profileId);

			// Borramos
			this.profileService.delete(p);

			// Comprobamos que se ha borrado
			Assert.isNull(this.profileService.findOne(p.getId()));

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverListProfileTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			//  Se accede con admin -> true
			{
				"admin1", null
			},
			//  Se accede con customer -> true
			{
				"customer1", null
			},
			//  Se accede con handyworker -> true
			{
				"handyworker1", null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listProfileTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverRegisterProfileTest() {

		final Object testingData[][] = {
			// Registrar profile sin estar autentificado -> false
			{
				null, "nickTest", "socialNetworkTest", "www.linktest.com", IllegalArgumentException.class
			},
			// Registrar profile con datos no v�lidos -> false
			{
				"handyworker1", "nickTest", null, "www.linktest.com", IllegalArgumentException.class
			},
			// Registrar profile con datos no v�lidos -> false
			{
				"handyworker1", "nickTest", "socialNetworktest", "thisisnotaurl", IllegalArgumentException.class
			},
			// Registrar profile autentificado como handyworker -> true
			{
				"handyworker1", "nickTest", "socialNetworkTest", "www.linktest.com", null
			},
			// Registrar profile autentificado como sponsor -> true
			{
				"sponsor1", "nickTest", "socialNetworkTest", "www.linktest.com", null
			},
			// Registrar profile autentificado como customer -> true
			{
				"customer1", "nickTest", "socialNetworkTest", "www.linktest.com", null
			},
			// Registrar profile autentificado como admin -> true
			{
				"admin", "nickTest", "socialNetworkTest", "www.linktest.com", null
			},
			// Registrar profile autentificado como referee -> true
			{
				"referee1", "nickTest", "socialNetworkTest", "www.linktest.com", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerProfileTest((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (Class<?>) testingData[i][4]);
	}

	@Test
	public void driverDeleteProfileTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", 3131, null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", 3131, null
			},
			//  Se accede con admin -> true
			{
				"admin1", 3131, null
			},
			//  Se accede con customer -> true
			{
				"customer1", 3131, null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, 3131, IllegalArgumentException.class
			},
			//  Se borra una id que no existe -> false
			{
				"customer1", 1212112, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.deleteSponsorshipTest((String) testingData[i][0], (int) testingData[i][1], (Class<?>) testingData[i][2]);
	}
}
