
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import utilities.AbstractTest;
import domain.Sponsor;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class SponsorServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private SponsorService	sponsorService;

	@Autowired
	private ActorService	actorService;


	// Templates --------------------------------------------------------------

	/*
	 * "An actor who is authenticated as a sponsor must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del sponsor no existe
	 */
	public void profileEdit(final String username, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber, final String address, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es sponsor
			this.sponsorService.checkIfSponsor();

			// Inicializamos los atributos para la edici�n
			Sponsor sponsor;

			sponsor = this.sponsorService.findOne(this.actorService.findByPrincipal().getId());

			sponsor.setName(name);
			sponsor.setMiddleName(middleName);
			sponsor.setSurname(surname);
			sponsor.setPhoto(photo);
			sponsor.setEmail(email);
			sponsor.setPhoneNumber(phoneNumber);
			sponsor.setAddress(address);

			// Comprobamos atributos
			this.sponsorService.checkSponsor(sponsor);

			// Guardamos
			this.sponsorService.save(sponsor);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * Vista para ver todos los Sponsors
	 * 
	 * En este caso de uso se listan los sponsors desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de sponsors sin autenticarnos.
	 */
	public void listSponsorTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.sponsorService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver los sponsorships que ha creado el sponsor logueado
	 * 
	 * 
	 * 
	 * En este caso de uso se listan los sponsorships desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de sponsorships sin autenticarnos.
	 */
	public void listSponsorshipsTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			final Sponsor sponsor = this.sponsorService.findByPrincipal();

			sponsor.getSponsorships();
			this.sponsorService.checkIfSponsor();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Registro de sponsor
	 * 
	 * En este caso de uso se llevara a cabo el registro de un sponsor en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � Nombre de usuario ya existente
	 */
	public void registerSponsor(final String username, final String newUsername, final String password, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber,
		final String address, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);
			Assert.isNull(this.actorService.findByPrincipal());

			// Inicializamos los atributos para la creaci�n
			final Sponsor sponsor = this.sponsorService.create();

			sponsor.setName(name);
			sponsor.setMiddleName(middleName);
			sponsor.setSurname(surname);
			sponsor.setPhoto(photo);
			sponsor.setEmail(email);
			sponsor.setPhoneNumber(phoneNumber);
			sponsor.setAddress(address);

			final UserAccount ua = sponsor.getUserAccount();
			ua.setUsername(newUsername);
			ua.setPassword(password);
			sponsor.setUserAccount(ua);

			// Comprobamos atributos
			this.sponsorService.checkSponsor(sponsor);

			// Guardamos
			this.sponsorService.save(sponsor);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	//	@Test
	//	public void driverProfileEdit() {
	//
	//		final Object testingData[][] = {
	//			// Editar profile con sponsor1 autentificado -> true
	//			{
	//				"sponsor1", "Sponsor1Name", "sponsor1middlename", "sponsor1surname", "www.linkedin.com/testpicture.jpg", "sponsor1@gmail.com", "612232332", "Abbey Road, 21", null
	//			},
	//			// Editar profile con sponsor2 autentificado -> true
	//			{
	//				"sponsor2", "Sponsor2Name", "sponsor2middlename", "sponsor2surname", "www.linkedin.com/testpicture.jpg", "sponsor2@gmail.com", "612231488", "Abbey Road, 21", null
	//			},
	//			// Editar profile sin autentificar -> false
	//			{
	//				null, "Sponsor1Name", "sponsor3middlename", "sponsor2surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612232332", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			// Editar profile con name no v�lido -> false
	//			{
	//				"sponsor1", null, "sponsor3middlename", "sponsor2surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612232332", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			// Editar profile con email no v�lido -> false
	//			{
	//				"sponsor1", "Sponsor1name", "sponsor3middlename", "sponsor2surname", "www.linkedin.com/testpicture.jpg", "sponsor3notemail", "612232332", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			// Editar profile con picture no v�lido -> false
	//			{
	//				"sponsor1", "Sponsor1name", "sponsor3middlename", "sponsor2surname", "thisisnotalink", "sponsor3@gmail.com", "612232332", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.profileEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
	//				(Class<?>) testingData[i][8]);
	//	}

	@Test
	public void driverListSponsorTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			//  Se accede con usuario no autentificado -> true
			{
				null, null
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listSponsorTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	@Test
	public void driverListSponsorshipsTest() {
		final Object testingData[][] = {
			// Se accede con sponsor -> true
			{
				"sponsor1", null
			},
			// Se accede con sponsor -> true
			{
				"sponsor2", null
			},
			// Se accede con admin -> false
			{
				"admin", IllegalArgumentException.class
			},

			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.listSponsorshipsTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//	@Test
	//	public void driverRegisterSponsor() {
	//
	//		final Object testingData[][] = {
	//			// Registrar sponsor sin estar autentificado -> true
	//			{
	//				null, "sponsor3", "sponsor3", "Sponsor3Name", "sponsor3middlename", "sponsor3surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612239922", "Abbey Road, 21", null
	//			},
	//			// Registrar sponsor sin estar autentificado -> true
	//			{
	//				null, "sponsor4", "sponsor4", "Sponsor4Name", "sponsor4middlename", "sponsor4surname", "www.linkedin.com/testpicture.jpg", "sponsor4@gmail.com", "612239922", "Abbey Road, 21", null
	//			},
	//			// Registrar sponsor con datos no v�lidos -> false
	//			{
	//				null, "sponsor4", "sponsor4", null, "sponsor4middlename", null, "www.linkedin.com/testpicture.jpg", "sponsor4@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			 //Registrar sponsor con datos no v�lidos -> false
	//			{
	//				null, "sponsor4", "sponsor4", "Sponsor4Name", "sponsor4middlename", "sponsor4surname", "thisisnotalink", "sponsor4@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			// Registrar sponsor con datos no v�lidos -> false
	//			{
	//				null, "sponsor4", "sponsor4", "Sponsor4Name", "sponsor4middlename", "sponsor4surname", "www.linkedin.com/testpicture.jpg", "thisisnotamail", "612239922", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			// Registrar sponsor estando autentificado -> false
	//			{
	//				"admin", "sponsor3", "sponsor3", "Sponsor3Name", "sponsor3middlename", "sponsor3surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//			// Registrar sponsor estando autentificado -> false
	//			{
	//				"customer1", "sponsor3", "sponsor3", "Sponsor3Name", "sponsor3middlename", "sponsor3surname", "www.linkedin.com/testpicture.jpg", "sponsor3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
	//			},
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.registerSponsor((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
	//				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	//	}
}
