
package services;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import security.UserAccount;
import utilities.AbstractTest;
import domain.Customer;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class CustomerServiceTest extends AbstractTest {

	// Services and repositories

	@Autowired
	private CustomerService	customerService;

	@Autowired
	private ActorService	actorService;


	/*
	 * "An actor who is authenticated as a customer must be able to:
	 * - Edit his or her profile."
	 * 
	 * En este caso de uso se llevar� a cabo la acci�n de editar un perfil.
	 * Para forzar el error pueden darse varios casos, como son:
	 * 
	 * � La persona no est� autentificada
	 * � Alguno de los atributos no tiene el formato correcto o no es v�lido
	 * � La Id del customer no existe
	 */

	//Tests -------------------------------------------

	public void profileEdit(final String username, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber, final String address, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos si la persona autentificada es sponsor
			this.customerService.checkIfCustomer();

			// Inicializamos los atributos para la edici�n
			Customer customer;

			customer = this.customerService.findOne(this.actorService.findByPrincipal().getId());

			customer.setName(name);
			customer.setMiddleName(middleName);
			customer.setSurname(surname);
			customer.setPhoto(photo);
			customer.setEmail(email);
			customer.setPhoneNumber(phoneNumber);
			customer.setAddress(address);

			// Comprobamos atributos
			this.customerService.checkCustomer(customer);

			// Guardamos
			this.customerService.save(customer);

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver todos los Customers
	 * 
	 * En este caso de uso se listan los customers desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de sponsors sin autenticarnos.
	 */
	public void listCustomerTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			this.customerService.findAll();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	/*
	 * Vista para ver las tasks que ha creado el customer logueado
	 * 
	 * 
	 * 
	 * En este caso de uso se listan las tasks desde su correspondiente vista
	 * Para provocar el error intentamos acceder
	 * al listado de tasks sin autenticarnos.
	 */

	public void listTasksTest(final String username, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);
			final Customer customer = this.customerService.findByPrincipal();

			customer.getTasks();
			this.customerService.checkIfCustomer();

			this.unauthenticate();

		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}
	/*
	 * Registro de customer
	 * 
	 * En este caso de uso se llevara a cabo el registro de un customer en el sistema
	 * Para forzar el error pueden darse varios casos:
	 * 
	 * � El usuario esta autentificado
	 * � Atributos del registro incorrectos
	 * � No aceptar las condiciones
	 * � Nombre de usuario ya existente
	 */

	public void registerCustomer(final String username, final String newUsername, final String password, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber,
		final String address, final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que no este autentificado
			Assert.isTrue(username == null);
			Assert.isNull(this.actorService.findByPrincipal());

			// Inicializamos los atributos para la creaci�n
			final Customer customer = this.customerService.create();

			customer.setName(name);
			customer.setMiddleName(middleName);
			customer.setSurname(surname);
			customer.setPhoto(photo);
			customer.setEmail(email);
			customer.setPhoneNumber(phoneNumber);
			customer.setAddress(address);

			final UserAccount ua = customer.getUserAccount();
			ua.setUsername(newUsername);
			ua.setPassword(password);
			customer.setUserAccount(ua);

			// Comprobamos atributos
			this.customerService.checkCustomer(customer);

			// Guardamos
			this.customerService.save(customer);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	// Drivers ----------------------------------------------------------------------

	@Test
	public void driverProfileEdit() {

		final Object testingData[][] = {
			// Editar profile con customer1 autentificado -> true
			{
				"customer1", "Customer1Name", "customer1middlename", "customer1surname", "www.linkedin.com/testpicture.jpg", "customer1@gmail.com", "612232332", "Abbey Road, 21", null
			},
			// Editar profile con customer2 autentificado -> true
			{
				"customer2", "Customer2Name", "customer2middlename", "customer2surname", "www.linkedin.com/testpicture.jpg", "customer2@gmail.com", "612231488", "Abbey Road, 21", null
			},
			// Editar profile sin autentificar -> false
			{
				null, "Customer1Name", "customer3middlename", "customer2surname", "www.linkedin.com/testpicture.jpg", "customer3@gmail.com", "612232332", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Editar profile con name no v�lido -> false
			{
				"customer1", null, "customer3middlename", "customer2surname", "www.linkedin.com/testpicture.jpg", "customer3@gmail.com", "612232332", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Editar profile con email no v�lido -> false
			{
				"customer", "Customer1name", "customer3middlename", "customer2surname", "www.linkedin.com/testpicture.jpg", null, "612232332", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Editar profile con picture no v�lido -> false
			{
				"customer1", "Customer1name", "customer3middlename", "customer2surname", null, "customer3@gmail.com", "612232332", "Abbey Road, 21", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.profileEdit((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(Class<?>) testingData[i][8]);
	}

	@Test
	public void driverListCustomerTest() {
		final Object testingData[][] = {
			// Se accede con admin -> true
			{
				"admin", null
			},
			// Se accede con customer -> true
			{
				"customer1", null
			},
			//  Se accede con usuario no autentificado -> false
			{
				null, IllegalArgumentException.class
			}
		};
		for (int i = 0; i < testingData.length; i++)
			this.listCustomerTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	}

	//	@Test
	//	public void driverListTasksTest() {
	//		final Object testingData[][] = {
	//			// Se accede con customer -> true
	//			{
	//				"customer1", null
	//			},
	//			// Se accede con customer -> true
	//			{
	//				"customer2", null
	//			},
	//			// Se accede con admin -> false
	//			{
	//				"admin", IllegalArgumentException.class
	//			},
	//
	//			//  Se accede con usuario no autentificado -> false
	//			{
	//				null, IllegalArgumentException.class
	//			},
	//		};
	//		for (int i = 0; i < testingData.length; i++)
	//			this.listTasksTest((String) testingData[i][0], (Class<?>) testingData[i][1]);
	//	}

	@Test
	public void driverRegisterCustomer() {

		final Object testingData[][] = {
			// Registrar customer sin estar autentificado -> true
			{
				null, "customer3", "customer3", "Customer3Name", "customer3middlename", "customer3surname", "www.linkedin.com/testpicture.jpg", "customer3@gmail.com", "612239922", "Abbey Road, 21", null
			},
			// Registrar customer sin estar autentificado -> true
			{
				null, "customer4", "customer4", "Customer4Name", "customer4middlename", "customer4surname", "www.linkedin.com/testpicture.jpg", "customer4@gmail.com", "612239922", "Abbey Road, 21", null
			},
			// Registrar customer con datos no v�lidos -> false
			{
				null, "customer4", "customer4", null, "customer4middlename", null, "www.linkedin.com/testpicture.jpg", "customer4@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar customer con datos no v�lidos -> false
			{
				null, "customer4", "customer4", "Customer4Name", "customer4middlename", "customer4surname", "thisisnotalink", "customer4@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar customer con datos no v�lidos -> false
			{
				null, "customer4", "customer4", "Customer4Name", "customer4middlename", "customer4surname", "www.linkedin.com/testpicture.jpg", "thisisnotamail", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar customer estando autentificado -> false
			{
				"admin", "customer3", "customer3", "Customer3Name", "customer3middlename", "customer3surname", "www.linkedin.com/testpicture.jpg", "customer3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar customer estando autentificado -> false
			{
				"customer1", "customer3", "customer3", "Customer3Name", "customer3middlename", "customer3surname", "www.linkedin.com/testpicture.jpg", "customer3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.registerCustomer((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6],
				(String) testingData[i][7], (String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}
}
