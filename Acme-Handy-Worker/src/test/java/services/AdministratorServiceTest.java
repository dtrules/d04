
package services;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import security.Authority;
import security.UserAccount;
import utilities.AbstractTest;
import domain.Administrator;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
	"classpath:spring/datasource.xml", "classpath:spring/config/packages.xml"
})
@Transactional
public class AdministratorServiceTest extends AbstractTest {

	//Service under test ---------------------------

	@Autowired
	private AdministratorService	administratorService;

	@Autowired
	private ActorService			actorService;


	public void createAdmin(final String username, final String newUsername, final String password, final String name, final String middleName, final String surname, final String photo, final String email, final String phoneNumber, final String address,
		final Class<?> expected) {

		Class<?> caught = null;

		try {

			this.authenticate(username);

			// Comprobamos que este autentificado como admin
			this.actorService.checkAuth(Authority.ADMIN);

			// Inicializamos los atributos para la creaci�n
			final Administrator administrator = this.administratorService.create();

			administrator.setName(name);
			administrator.setMiddleName(middleName);
			administrator.setSurname(surname);
			administrator.setPhoto(photo);
			administrator.setEmail(email);
			administrator.setPhoneNumber(phoneNumber);
			administrator.setAddress(address);

			final UserAccount ua = administrator.getUserAccount();
			ua.setUsername(newUsername);
			ua.setPassword(password);
			administrator.setUserAccount(ua);

			// Comprobamos atributos
			this.administratorService.checkAdministrator(administrator);

			// Guardamos
			this.administratorService.save(administrator);

			this.unauthenticate();
		} catch (final Throwable oops) {

			caught = oops.getClass();

		}

		this.checkExceptions(expected, caught);
	}

	@Test
	public void driverCreateAdmin() {

		final Object testingData[][] = {
			// Crear admin sin estar autentificado -> false
			{
				null, "admin2", "admin2", "Admin2Name", "Admin2Middlename", "Admin2Surname", "www.linkedin.com/testpicture.jpg", "admin@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar admin con datos no v�lidos -> false
			{
				null, "admin3", "admin3", null, "admin3middlename", null, "www.linkedin.com/testpicture.jpg", "admin3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar admin con datos no v�lidos -> false
			{
				null, "admin3", "admin3", "Admin3Name", "Admin3middlename", "Admin3surname", "thisisnotalink", "admin3@gmail.com", "612239922", "Abbey Road, 21", IllegalArgumentException.class
			},
			// Registrar admin estando autentificado -> true
			{
				"admin", "admin2", "admin2", "Admin2Name", "Admin2middlename", "Admin2surname", "www.linkedin.com/testpicture.jpg", "admin2@gmail.com", "612239922", "Abbey Road, 21", null
			},
		};
		for (int i = 0; i < testingData.length; i++)
			this.createAdmin((String) testingData[i][0], (String) testingData[i][1], (String) testingData[i][2], (String) testingData[i][3], (String) testingData[i][4], (String) testingData[i][5], (String) testingData[i][6], (String) testingData[i][7],
				(String) testingData[i][8], (String) testingData[i][9], (Class<?>) testingData[i][10]);
	}

}
