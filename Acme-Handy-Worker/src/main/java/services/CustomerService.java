
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CustomerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Actor;
import domain.Customer;
import domain.Profile;
import domain.Task;

@Service
@Transactional
public class CustomerService {

	//Managed repository ------------------------------------------------

	@Autowired
	private CustomerRepository	customerRepository;

	// Supporting services ---------------------------------------------------------

	@Autowired
	private FolderService		folderService;
	@Autowired
	private ActorService		actorService;


	// Constructor methods ---------------------------------------------------------

	public CustomerService() {
		super();
	}

	public Customer create() {

		final Customer res = new Customer();

		final UserAccount userAccount = new UserAccount();

		final Authority authority = new Authority();
		authority.setAuthority(Authority.CUSTOMER);

		Collection<Authority> authorities;

		authorities = userAccount.getAuthorities();
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		final Collection<Profile> profiles = new ArrayList<Profile>();
		res.setProfiles(profiles);
		res.setIsBanned(false);
		res.setIsSuspicious(false);
		res.setFolders(this.folderService.createSystemFolders());

		final Collection<Task> tasks = new ArrayList<Task>();
		res.setTasks(tasks);

		return res;
	}

	public void save(final Customer customer) {
		Assert.notNull(customer);
		this.checkCustomer(customer);
		this.customerRepository.save(customer);
	}
	public Customer findOne(final int customerId) {
		Assert.isTrue(customerId != 0);
		Customer result;

		result = this.customerRepository.findOne(customerId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Customer> findAll() {
		Collection<Customer> result;

		Assert.isTrue(this.actorService.findByPrincipal() != null);

		result = this.customerRepository.findAll();
		Assert.notNull(result);

		return result;
	}
	// Finds the actual customer
	public Customer findByPrincipal() {
		Customer result;

		final Actor principal = this.actorService.findByPrincipal();

		result = this.customerRepository.findOne(principal.getId());
		Assert.notNull(result);

		return result;
	}

	// Find a customer by his or her username
	public Customer findSponsorByUsername(final String username) {
		return this.customerRepository.findCustomerByUsername(username);
	}

	// Find customer by useraccount
	public Customer findCustomerByUserAccount(final UserAccount userAccount) {
		return this.customerRepository.findByUserAccount(userAccount);
	}
	//list of customers who have pusblished at least 10% more fix-up tasks
	public Collection<Customer> customerListPublishedTasks() {
		return this.customerRepository.customerListPublishedTasks();
	}

	//top-three customers in terms of the total number of complaints.
	public Collection<Customer> top3CustomerByComplaint() {
		return this.customerRepository.top3CustomerByComplaint();
	}
	//Check customer
	public void checkCustomer(final Customer customer) {
		Boolean result = true;

		if (customer.getAddress() == null || customer.getFolders() == null || customer.getName() == null || customer.getSurname() == null)
			result = false;

		Assert.isTrue(result);
	}

	public void checkIfCustomer() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.CUSTOMER))
				res = true;
		Assert.isTrue(res);
	}

}
