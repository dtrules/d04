
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.CustomerEndorsementRepository;
import security.Authority;
import domain.CustomerEndorsement;
import domain.Customization;

@Service
@Transactional
public class CustomerEndorsementService {

	//Managed Repository------------------------------

	@Autowired
	private CustomerEndorsementRepository	customerEndorsementRepository;

	//Supporting Services-----------------------------
	@Autowired
	private ActorService					actorService;

	@Autowired
	private CustomizationService			customizationService;


	// Constructor methods ---------------------------------------------------------
	public CustomerEndorsementService() {
		super();
	}

	public CustomerEndorsement create() {

		final Collection<String> comments = new ArrayList<String>();
		this.actorService.checkAuth(Authority.CUSTOMER);

		final CustomerEndorsement res = new CustomerEndorsement();
		res.setComments(comments);

		return res;
	}

	public CustomerEndorsement save(final CustomerEndorsement customerEndorsement) {
		Assert.notNull(customerEndorsement);
		this.actorService.checkAuth(Authority.CUSTOMER);
		final Date moment = new Date(System.currentTimeMillis() - 1000);

		customerEndorsement.setMoment(moment);
		CustomerEndorsement result = new CustomerEndorsement();
		if (customerEndorsement.getHandyWorker() != null)
			result = this.customerEndorsementRepository.save(customerEndorsement);

		return result;
	}

	public void delete(final CustomerEndorsement customerEndorsement) {
		Assert.notNull(customerEndorsement);
		this.actorService.checkAuth(Authority.CUSTOMER);
		this.customerEndorsementRepository.delete(customerEndorsement);

	}

	public CustomerEndorsement findOne(final int customerEndorsementId) {
		Assert.isTrue(customerEndorsementId != 0);
		final CustomerEndorsement result = this.customerEndorsementRepository.findOne(customerEndorsementId);
		Assert.notNull(result);

		return result;
	}

	public Collection<CustomerEndorsement> findAll() {
		final Collection<CustomerEndorsement> result = this.customerEndorsementRepository.findAll();

		return result;
	}

	//Score

	public double obtainScore(final CustomerEndorsement customerEndorsement) {
		double res = 0.0;
		final Customization c = this.customizationService.findCustomization();
		double positiveWords = 0.0;
		double negativeWords = 0.0;
		for (final String s : c.getPositiveWords())
			for (final String s1 : customerEndorsement.getComments())
				if (s1.toLowerCase().contains(s.toLowerCase()))
					positiveWords++;
		for (final String s : c.getNegativeWords())
			for (final String s1 : customerEndorsement.getComments())
				if (s1.toLowerCase().contains(s.toLowerCase()))
					negativeWords++;
		res = positiveWords - negativeWords;
		return res;
	}

	//Query

	public Collection<CustomerEndorsement> getEndorsementsFromHandyWorker(final int handyWorkerId) {
		return this.customerEndorsementRepository.getEndorsementsFromHandyWorker(handyWorkerId);
	}

}
