
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SponsorRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Profile;
import domain.Sponsor;
import domain.Sponsorship;

@Service
@Transactional
public class SponsorService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private SponsorRepository	sponsorRepository;

	// Supporting services ---------------------------------------------------------
	@Autowired
	private FolderService		folderService;


	// Constructor methods ---------------------------------------------------------
	public SponsorService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------------

	public Sponsor create() {
		final Sponsor s = new Sponsor();
		final UserAccount ua = new UserAccount();

		final Authority a = new Authority();
		a.setAuthority(Authority.SPONSOR);

		Collection<Authority> authorities;

		authorities = ua.getAuthorities();
		authorities.add(a);
		ua.setAuthorities(authorities);

		final Collection<Profile> profiles = new ArrayList<Profile>();
		s.setProfiles(profiles);

		s.setFolders(this.folderService.createSystemFolders());

		final Collection<Sponsorship> sponsorships = new ArrayList<Sponsorship>();
		s.setSponsorships(sponsorships);
		s.setIsBanned(false);
		s.setIsSuspicious(false);

		return s;
	}

	public Sponsor findOne(final int sponsorId) {
		Assert.isTrue(sponsorId != 0);
		Sponsor result;

		result = this.sponsorRepository.findOne(sponsorId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Sponsor> findAll() {

		Collection<Sponsor> result;

		result = this.sponsorRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public void save(final Sponsor sponsor) {
		Assert.notNull(sponsor);
		this.checkSponsor(sponsor);

	}

	// ***************************************

	// Check if the actual user is a sponsor
	public void checkIfSponsor() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.SPONSOR))
				res = true;
		Assert.isTrue(res);
	}

	// Finds the actual sponsor
	public Sponsor findByPrincipal() {
		Sponsor result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.sponsorRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	// Find a sponsor by his or her username
	public Sponsor findSponsorByUsername(final String username) {
		return this.sponsorRepository.findSponsorByUsername(username);
	}

	// Find sponsor by useraccount
	public Sponsor findSponsorByUserAccount(final UserAccount userAccount) {
		return this.sponsorRepository.findByUserAccount(userAccount);
	}

	//Check sponsor
	public void checkSponsor(final Sponsor sponsor) {
		Boolean result = true;

		if (sponsor.getEmail() == null || sponsor.getFolders() == null || sponsor.getName() == null || sponsor.getSurname() == null)
			result = false;

		Assert.isTrue(result);
	}

}
