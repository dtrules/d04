
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.NoteRepository;
import security.Authority;
import domain.Actor;
import domain.Application;
import domain.Complaint;
import domain.Customer;
import domain.HandyWorker;
import domain.Note;
import domain.Referee;
import domain.Report;

@Service
@Transactional
public class NoteService {

	//Managed Repository------------------------------

	@Autowired
	private NoteRepository		noteRepository;

	//Supporting Services-----------------------------

	@Autowired
	private ActorService		actorService;

	@Autowired
	private CustomerService		customerService;

	@Autowired
	private RefereeService		refereeService;

	@Autowired
	private HandyWorkerService	handyWorkerService;

	@Autowired
	private ComplaintService	complaintService;


	// Constructor methods ---------------------------------------------------------
	public NoteService() {
		super();
	}

	public Note create() {
		this.actorService.checkAuth(Authority.CUSTOMER, Authority.REFEREE, Authority.HANDYWORKER);
		Note result;
		Date moment;
		Collection<String> handyWorkerComments, refereeComments, customerComments;

		result = new Note();
		moment = new Date(System.currentTimeMillis() - 1000);
		handyWorkerComments = Collections.<String> emptySet();
		refereeComments = Collections.<String> emptySet();
		customerComments = Collections.<String> emptySet();

		result.setMoment(moment);
		result.setHandyWorkerComments(handyWorkerComments);
		result.setRefereeComments(refereeComments);
		result.setCustomerComments(customerComments);

		Assert.notNull(result);

		return result;
	}

	public Collection<Note> findAll() {
		Collection<Note> result;

		result = this.noteRepository.findAll();

		Assert.notNull(result);

		return result;

	}
	public Note findOne(final int noteId) {
		Assert.isTrue(noteId != 0);

		Note result;
		result = this.noteRepository.findOne(noteId);

		Assert.notNull(result);

		return result;
	}

	public Note save(final Note note, final Report r) {
		Assert.notNull(note);
		this.checkNote(note);

		final Date moment = new Date(System.currentTimeMillis() - 1000);
		note.setMoment(moment);

		Note result;
		for (final String s : note.getCustomerComments())
			this.actorService.checkSpamWords(s);

		for (final String t : note.getHandyWorkerComments()) {
			this.actorService.checkSpamWords(t);

			for (final String w : note.getRefereeComments())
				this.actorService.checkSpamWords(w);
		}
		result = this.noteRepository.save(note);

		return result;
	}

	public void delete(final Note note) {
		Assert.notNull(note);
		Assert.isTrue(note.getId() != 0);
		Assert.isTrue(this.noteRepository.exists(note.getId()));

		this.noteRepository.delete(note);
	}

	//Check Note
	public void checkNote(final Note note) {
		Boolean result = true;

		if (note.getMoment() == null)
			result = false;

		Assert.isTrue(result);
	}
	public Collection<Note> findNotesByComplaintId(final int complaintId) {
		return this.noteRepository.findNotesByComplaintId(complaintId);
	}

	public Collection<Note> getNotes() {
		this.actorService.checkAuth(Authority.CUSTOMER, Authority.HANDYWORKER, Authority.REFEREE);
		final Actor principal = this.actorService.findByPrincipal();
		final Collection<Note> result = new ArrayList<Note>();

		if (this.customerService.findOne(principal.getId()) != null) {
			final Customer c = this.customerService.findByPrincipal();
			final Collection<Complaint> co = this.complaintService.findComplaintByCustomer(c.getId());
			for (final Complaint com : co)
				result.addAll(this.findNotesByComplaintId(com.getId()));
		} else if (this.refereeService.findOne(principal.getId()) != null) {
			final Referee r = this.refereeService.findByPrincipal();
			for (final Report report : r.getReports())
				result.addAll(report.getNotes());
		} else if (this.handyWorkerService.findOne(principal.getId()) != null) {
			final HandyWorker hw = this.handyWorkerService.findByPrincipal();
			for (final Application a : hw.getApplications()) {
				final Collection<Complaint> complaints = a.getTask().getComplaints();
				for (final Complaint complaint : complaints)
					result.addAll(this.findNotesByComplaintId(complaint.getId()));
			}

		}
		return result;

	}

}
