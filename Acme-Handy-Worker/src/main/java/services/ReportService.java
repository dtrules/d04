
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.ReportRepository;
import security.Authority;
import domain.Note;
import domain.Report;

@Service
@Transactional
public class ReportService {

	//Managed Repository------------------------------

	@Autowired
	private ReportRepository	reportRepository;

	//Supporting Services-----------------------------

	@Autowired
	private ActorService		actorService;


	// Constructor methods ---------------------------------------------------------
	public ReportService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Report create() {

		this.actorService.checkAuth(Authority.REFEREE);

		final Report r = new Report();

		final Collection<String> attachments = new ArrayList<String>();
		r.setAttachments(attachments);

		final Collection<Note> notes = new ArrayList<Note>();
		r.setNotes(notes);

		return r;
	}

	public Report save(final Report r) {
		Assert.notNull(r);
		this.checkReport(r);

		final Date moment = new Date(System.currentTimeMillis() - 1000);
		r.setMoment(moment);

		Report result;
		for (final String s : r.getAttachments())
			this.actorService.checkSpamWords(s);

		this.actorService.checkSpamWords(r.getDescription());
		result = this.reportRepository.save(r);

		return result;
	}

	public void delete(final Report t) {
		Assert.notNull(t);
		Assert.isTrue(t.getId() != 0);

		final Report result = this.reportRepository.findOne(t.getId());

		this.reportRepository.delete(result);

	}

	public Report findOne(final int reportId) {
		Assert.isTrue(reportId != 0);
		Report result;

		result = this.reportRepository.findOne(reportId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Report> findAll() {

		Collection<Report> result;

		result = this.reportRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Report
	public void checkReport(final Report report) {
		Boolean result = true;

		if (report.getComplaint() == null || report.getMoment() == null || report.getDescription() == null)
			result = false;

		Assert.isTrue(result);
	}

	//Queries-----------------------------------------
	public Collection<Double> avgNotePerRefereeReport(final Report report) {
		return this.reportRepository.avgNotePerRefereeReport(report);
	}

}
