
package services;

import java.util.ArrayList;
import java.util.Collection;

import domain.Tutorial;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.SectionRepository;
import security.Authority;
import domain.Section;

@Service
@Transactional
public class SectionService {

	//Managed repository ------------------------------------------------

	@Autowired
	private SectionRepository	sectionRepository;

	//Supported services ------------------------------------------------
	@Autowired
	private ActorService		actorService;
	@Autowired
	private TutorialService tutorialService;



	// Constructor methods ---------------------------------------------------------
	public SectionService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Section create() {

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Section s = new Section();

		final Collection<String> pictures = new ArrayList<String>();

		s.setPictures(pictures);

		return s;
	}

	public Section save(final Section s) {
		Assert.notNull(s);
		this.checkSection(s);

		this.actorService.checkAuth(Authority.HANDYWORKER);

		Section result;
		this.actorService.checkSpamWords(s.getText());
		this.actorService.checkSpamWords(s.getTitle());
		for (final String t : s.getPictures())
			this.actorService.checkSpamWords(t);
		result = this.sectionRepository.save(s);

		return result;
	}

	public void delete(final Section s) {

		Assert.notNull(s);
		Assert.isTrue(s.getId() != 0);

		this.actorService.checkAuth(Authority.HANDYWORKER);

		Tutorial tutorial = tutorialService.findTutorialBySection(s.getId());

		// Los tutorials han de tener al menos 1 section, si no tuviera al menos m�s de 1, no se puede borrar la section
		Assert.isTrue(tutorial.getSections().size()>1);

		final Section result = this.sectionRepository.findOne(s.getId());

		this.sectionRepository.delete(result);

	}

	public Section findOne(final int sectionId) {
		Assert.isTrue(sectionId != 0);
		Section result;

		result = this.sectionRepository.findOne(sectionId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Section> findAll() {

		Collection<Section> result;

		result = this.sectionRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	//Check Section
	public void checkSection(final Section section) {
		Boolean result = true;

		if (section.getTitle() == null || section.getText() == null || section.getPictures() == null)
			result = false;

		Assert.isTrue(result);
	}

}
