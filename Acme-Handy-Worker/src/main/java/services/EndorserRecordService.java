
package services;

import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.EndorserRecordRepository;
import security.Authority;
import domain.EndorserRecord;

@Service
public class EndorserRecordService {

	//Managed repository -------------------------------------

	@Autowired
	private EndorserRecordRepository	endorserRecordRepository;

	//Supported Services -----------------------------------

	@Autowired
	private ActorService				actorService;


	// Constructor methods
	public EndorserRecordService() {
		super();
	}

	public EndorserRecord create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final EndorserRecord res = new EndorserRecord();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		Assert.notNull(res);

		return res;
	}

	public Collection<EndorserRecord> findAll() {
		final Collection<EndorserRecord> res = this.endorserRecordRepository.findAll();

		Assert.notNull(res);

		return res;

	}
	public EndorserRecord findOne(final int endorserRecordId) {
		Assert.isTrue(endorserRecordId > 0);

		final EndorserRecord res = this.endorserRecordRepository.findOne(endorserRecordId);

		Assert.notNull(res);

		return res;
	}

	public EndorserRecord save(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		this.checkEndorserRecord(endorserRecord);
		for (final String c : endorserRecord.getComments())
			this.actorService.checkSpamWords(c);

		this.actorService.checkSpamWords(endorserRecord.getEmail());
		this.actorService.checkSpamWords(endorserRecord.getLinkedinProfile());
		this.actorService.checkSpamWords(endorserRecord.getName());

		final EndorserRecord res = this.endorserRecordRepository.save(endorserRecord);

		return res;
	}

	public void delete(final EndorserRecord endorserRecord) {
		Assert.notNull(endorserRecord);
		this.actorService.checkAuth(Authority.HANDYWORKER);
		Assert.isTrue(endorserRecord.getId() > 0);
		Assert.isTrue(this.endorserRecordRepository.exists(endorserRecord.getId()));

		this.endorserRecordRepository.delete(endorserRecord);
	}

	//Check EndorserRecord
	public void checkEndorserRecord(final EndorserRecord endorserRecord) {
		Boolean res = true;

		if (endorserRecord.getComments() == null)
			res = false;

		Assert.isTrue(res);
	}
}
