
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.transaction.annotation.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import repositories.RefereeRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Profile;
import domain.Referee;
import domain.Report;

@Service
@Transactional
public class RefereeService {

	// Managed Repository ---------------------------------------------------------
	@Autowired
	private RefereeRepository	refereeRepository;

	// Supporting services ---------------------------------------------------------
	@Autowired
	private FolderService		folderService;


	// Constructor methods ---------------------------------------------------------
	public RefereeService() {
		super();
	}

	// Simple CRUD methods ---------------------------------------------------------

	public Referee create() {
		final Referee s = new Referee();
		final UserAccount ua = new UserAccount();

		final Authority a = new Authority();
		a.setAuthority(Authority.REFEREE);

		Collection<Authority> authorities;

		authorities = ua.getAuthorities();
		authorities.add(a);
		ua.setAuthorities(authorities);

		final Collection<Profile> profiles = new ArrayList<Profile>();
		s.setProfiles(profiles);

		s.setFolders(this.folderService.createSystemFolders());

		final Collection<Report> reports = new ArrayList<Report>();
		s.setReports(reports);
		s.setIsBanned(false);
		s.setIsSuspicious(false);

		return s;
	}

	public Referee findOne(final int refereeId) {
		Assert.isTrue(refereeId != 0);
		Referee result;

		result = this.refereeRepository.findOne(refereeId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Referee> findAll() {

		Collection<Referee> result;

		result = this.refereeRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	public Referee save(final Referee referee) {
		Assert.notNull(referee);
		this.checkReferee(referee);
		Referee result;

		result = this.refereeRepository.save(referee);

		return result;
	}

	// ***************************************

	// Check if the actual user is a referee
	public void checkIfReferee() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.REFEREE))
				res = true;
		Assert.isTrue(res);
	}

	// Finds the actual referee
	public Referee findByPrincipal() {
		Referee result;
		UserAccount userAccount;

		userAccount = LoginService.getPrincipal();
		Assert.notNull(userAccount);

		result = this.refereeRepository.findByUserAccount(userAccount);
		Assert.notNull(result);

		return result;
	}

	// Find a referee by his or her username
	public Referee findRefereeByUsername(final String username) {
		return this.refereeRepository.findRefereeByUsername(username);
	}

	// Find referee by useraccount
	public Referee findRefereeByUserAccount(final UserAccount userAccount) {
		return this.refereeRepository.findByUserAccount(userAccount);
	}

	// Find referee by report id
	public Referee findRefereeByReport(final int reportId) {
		return this.refereeRepository.findRefereeByReportId(reportId);
	}

	//Check Referee
	public void checkReferee(final Referee referee) {
		Boolean result = true;

		if (referee.getAddress() == null || referee.getFolders() == null || referee.getName() == null || referee.getSurname() == null)
			result = false;

		Assert.isTrue(result);
	}

}
