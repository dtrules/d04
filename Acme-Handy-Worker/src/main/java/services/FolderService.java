
package services;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.FolderRepository;
import domain.Actor;
import domain.Folder;
import domain.Message;

@Service
@Transactional
public class FolderService {

	//Managed repositories--------------------------------------
	@Autowired
	private FolderRepository	folderRepository;

	//Supported services----------------------------------------
	@Autowired
	private ActorService		actorService;

	@Autowired
	private MessageService		messageService;


	//Constructor-----------------------------------------------
	public FolderService() {
		super();
	}

	//Simple CRUD methods---------------------------------------
	public Folder create() {
		final Folder res = new Folder();
		final Actor a = this.actorService.findByPrincipal();
		final Collection<Folder> folders = a.getFolders();
		res.setMessages(new ArrayList<Message>());
		folders.add(res);
		return res;
	}

	public Folder findOne(final int folderId) {
		Assert.isTrue(folderId != 0);
		Folder res;
		res = this.folderRepository.findOne(folderId);
		Assert.notNull(res);
		return res;
	}

	public Collection<Folder> findAll() {
		Collection<Folder> res;
		res = this.folderRepository.findAll();
		Assert.notNull(res);
		return res;
	}

	public Folder save(final Folder folder) {
		Assert.notNull(folder);
		Assert.isTrue(this.checkNotSystemFolder(folder));
		Assert.isTrue(this.actorService.findByPrincipal().getId() == this.findActorByFolder(folder).getId());
		this.actorService.checkSpamWords(folder.getName());
		final Folder res = this.folderRepository.save(folder);
		return res;
	}

	public void delete(final Folder folder) {
		final Folder old = this.findOne(folder.getId());
		Assert.notNull(old);
		Assert.isTrue(this.checkNotSystemFolder(old));
		Assert.isTrue(this.actorService.findByPrincipal().getId() == this.findActorByFolder(folder).getId());
		for (final Message m : old.getMessages())
			this.messageService.delete(m);
		this.folderRepository.delete(folder);
	}

	public void delete(final int folderId) {
		Assert.notNull(folderId);
		final Folder folder = this.folderRepository.findOne(folderId);
		Assert.notNull(folder);
		Assert.isTrue(this.actorService.findByPrincipal().getId() == this.findActorByFolder(folder).getId());
		Assert.isTrue(this.checkNotSystemFolder(folder));
		for (final Message m : folder.getMessages())
			this.messageService.delete(m);
		folder.getMessages().clear();
		final Folder saved = this.save(folder);
		this.folderRepository.flush();
		this.folderRepository.delete(saved);
	}

	//Other methods---------------------------------------------

	public Collection<Folder> createSystemFolders() {
		final Collection<Folder> res = new ArrayList<Folder>();

		final Folder inbox = this.create();
		final Folder outbox = this.create();
		final Folder spambox = this.create();
		final Folder trashbox = this.create();

		inbox.setName("Inbox");
		outbox.setName("Outbox");
		spambox.setName("Spam");
		trashbox.setName("Trash");

		res.add(inbox);
		res.add(outbox);
		res.add(spambox);
		res.add(trashbox);

		return res;
	}

	public Collection<Folder> findFoldersByActorId(final int actorId) {
		Collection<Folder> res;
		Assert.notNull(actorId);
		Assert.isTrue(actorId > 0);
		res = this.actorService.findOne(actorId).getFolders();
		;
		Assert.notNull(res);
		return res;
	}

	public Folder findFolderByNameAndActorId(final int actorId, final String folderName) {
		Folder res;
		Assert.notNull(actorId);
		Assert.isTrue(actorId > 0);
		Assert.notNull(folderName);
		res = this.folderRepository.findFolderByNameAndActorId(actorId, folderName);
		Assert.notNull(res);
		return res;
	}

	public Actor findActorByFolder(final Folder folder) {
		Actor res;
		Assert.notNull(folder);
		res = this.folderRepository.findActorByFolderId(folder.getId());
		Assert.notNull(res);
		return res;
	}

	public boolean checkIfSystemFolder(final Folder folder) {
		boolean result = false;
		final String name = folder.getName();
		if (name.equals("Inbox") || name.equals("Outbox") || name.equals("Spam") || name.equals("Trash") || name.equals("inbox") || name.equals("outbox") || name.equals("spambox") || name.equals("trash"))
			result = true;
		return result;
	}

	public boolean checkNotSystemFolder(final Folder folder) {
		boolean result = true;
		if (this.checkIfSystemFolder(folder))
			result = false;
		return result;
	}
}
