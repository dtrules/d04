
package services;

import java.util.Collection;
import java.util.Collections;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.ApplicationRepository;
import security.Authority;
import domain.Application;
import domain.Status;
import domain.Task;

@Service
@Transactional
public class ApplicationService {

	//Managed repository ----------------------------

	@Autowired
	private ApplicationRepository	applicationRepository;

	//Supported Services ----------------------------

	@Autowired
	private ActorService			actorService;


	// Contructor methods
	public ApplicationService() {
		super();
	}

	// Simple CRUD methods

	public Application create() {
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Application res = new Application();

		final Collection<String> comments = Collections.<String> emptySet();
		res.setComments(comments);

		final Date moment = new Date();
		res.setMoment(moment);

		final Status status = new Status();
		res.setStatus(status);

		final Task task = new Task();
		res.setTask(task);

		Assert.notNull(res);
		this.checkApplication(res);

		return res;
	}
	
	public Collection<Application> findAll() {
		final Collection<Application> res = this.applicationRepository.findAll();

		Assert.notNull(res);

		return res;
	}

	public Application findOne(final int applicationId) {
		Assert.isTrue(applicationId > 0);

		final Application res = this.applicationRepository.findOne(applicationId);

		Assert.notNull(res);

		return res;
	}

	public Application save(final Application application) {
		Assert.notNull(application);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		this.checkApplication(application);
		final Application res = this.applicationRepository.save(application);

		return res;
	}
	
	public void delete(final Application application) {
		Assert.notNull(application);
		Assert.isTrue(application.getId() != 0);
		Assert.isTrue(this.applicationRepository.exists(application.getId()));

		this.applicationRepository.delete(application);
	}

	//Check Application
	public void checkApplication(final Application application) {
		Boolean res = true;

		if (application.getStatus() == null || application.getTask() == null || application.getComments() == null || application.getMoment() == null)
			res = false;

		Assert.isTrue(res);
	}

	public Application getApplicationByTask(final int taskId) {
		return this.applicationRepository.getApplicationByTask(taskId);
	}
}
