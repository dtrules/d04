
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.CurriculumRepository;
import repositories.HandyWorkerRepository;
import security.Authority;
import security.LoginService;
import security.UserAccount;
import domain.Application;
import domain.Curriculum;
import domain.Finder;
import domain.HandyWorker;
import domain.HandyWorkerEndorsement;
import domain.Profile;
import domain.Tutorial;

@Service
@Transactional
public class HandyWorkerService {

	//Managed repository --------------------------------

	@Autowired
	private HandyWorkerRepository	handyWorkerRepository;
	@Autowired
	private CurriculumRepository	curriculumRepository;

	//Supported Services ----------------------------------

	@Autowired
	private FolderService			folderService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private CurriculumService		curriculumService;


	// Contructor methods
	public HandyWorkerService() {
		super();
	}
	// Simple CRUD methods

	public Collection<HandyWorker> findAll() {
		final Collection<HandyWorker> res = this.handyWorkerRepository.findAll();

		Assert.notNull(res);

		return res;
	}

	public HandyWorker findOne(final int handyWorkerId) {
		Assert.isTrue(handyWorkerId > 0);

		final HandyWorker res = this.handyWorkerRepository.findOne(handyWorkerId);

		Assert.notNull(res);

		return res;
	}

	public HandyWorker save(final HandyWorker handyWorker) {
		Assert.notNull(handyWorker);
		this.actorService.checkAuth(Authority.HANDYWORKER);

		this.checkHandyWorker(handyWorker);

		final HandyWorker res = this.handyWorkerRepository.save(handyWorker);

		return res;
	}

	public HandyWorker create() {

		final HandyWorker res = new HandyWorker();
		final UserAccount userAccount = new UserAccount();
		final Authority authority = new Authority();

		authority.setAuthority(Authority.HANDYWORKER);
		Collection<Authority> authorities;
		authorities = userAccount.getAuthorities();
		authorities.add(authority);
		userAccount.setAuthorities(authorities);

		final Collection<HandyWorkerEndorsement> handyWorkerEndorsements = Collections.<HandyWorkerEndorsement> emptySet();
		res.setHandyWorkerEndorsements(handyWorkerEndorsements);

		final Finder finder = new Finder();
		res.setFinder(finder);

		final Curriculum curriculum = new Curriculum();
		res.setCurriculum(curriculum);

		final Collection<Tutorial> tutorials = Collections.<Tutorial> emptySet();
		res.setTutorials(tutorials);

		final Collection<Application> applications = Collections.<Application> emptySet();
		res.setApplications(applications);

		final Collection<Profile> profiles = new ArrayList<Profile>();
		res.setProfiles(profiles);

		res.setFolders(this.folderService.createSystemFolders());
		res.setIsBanned(false);
		res.setIsSuspicious(false);

		Assert.notNull(res);

		return res;
	}

	public void delete(final HandyWorker handyWorker) {
		Assert.notNull(handyWorker);
		Assert.isTrue(handyWorker.getId() != 0);
		Assert.isTrue(this.handyWorkerRepository.exists(handyWorker.getId()));

		this.actorService.checkAuth(Authority.HANDYWORKER);

		final HandyWorker hw = this.handyWorkerRepository.findOne(handyWorker.getId());

		if (this.curriculumRepository.exists(hw.getCurriculum().getId()))
			this.curriculumService.delete(hw.getCurriculum());

		this.handyWorkerRepository.delete(handyWorker);
	}

	// Finds the actual handyWorker
	public HandyWorker findByPrincipal() {
		HandyWorker result;

		result = this.findOne(this.actorService.findByPrincipal().getId());
		Assert.notNull(result);

		return result;
	}

	// Find handyWorker by useraccount
	public HandyWorker findCustomerByUserAccount(final UserAccount userAccount) {
		return this.handyWorkerRepository.findByUserAccount(userAccount);
	}

	//Check handyWorker
	public void checkHandyWorker(final HandyWorker handyWorker) {
		Boolean result = true;

		if (handyWorker.getApplications() == null || handyWorker.getFolders() == null || handyWorker.getCurriculum() == null || handyWorker.getFinder() == null || handyWorker.getHandyWorkerEndorsements() == null)
			result = false;

		Assert.isTrue(result);
	}

	public void checkIfHandyWorker() {
		boolean res = false;

		Collection<Authority> authority;
		authority = LoginService.getPrincipal().getAuthorities();
		for (final Authority a : authority)
			if (a.getAuthority().equals(Authority.HANDYWORKER))
				res = true;
		Assert.isTrue(res);
	}
}
