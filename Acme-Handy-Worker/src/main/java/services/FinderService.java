
package services;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import repositories.FinderRepository;
import security.Authority;
import domain.Customization;
import domain.Finder;
import domain.Task;

@Service
@Transactional
public class FinderService {

	//Managed repository ------------------------------------------------

	@Autowired
	private FinderRepository		finderRepository;

	//Suported services ------------------------------------------------

	@Autowired
	private HandyWorkerService		handyWorkerService;

	@Autowired
	private ActorService			actorService;

	@Autowired
	private CustomizationService	customizationService;


	// Constructor methods ---------------------------------------------------------
	public FinderService() {
		super();
	}

	//Simple CRUD methods ------------------------

	public Finder save(final Finder f) {
		Assert.notNull(f);
		this.checkFinder(f);

		//this.handyWorkerService.checkIfHandyWorker();
		this.actorService.checkAuth(Authority.HANDYWORKER);

		final Date moment = new Date(System.currentTimeMillis() - 1000);

		f.setMoment(moment);

		//		final HandyWorker hw = this.handyWorkerService.findOne(this.actorService.findByPrincipal().getId());
		//		hw.setFinder(f);

		Finder result;

		result = this.finderRepository.save(f);

		return result;
	}

	public Finder findOne(final int finderId) {
		Assert.isTrue(finderId != 0);
		Finder result;

		result = this.finderRepository.findOne(finderId);
		Assert.notNull(result);

		return result;
	}

	public Collection<Finder> findAll() {

		Collection<Finder> result;

		result = this.finderRepository.findAll();
		Assert.notNull(result);

		return result;
	}

	// Other business methods ---------------------------------------------------------

	//Check Finder
	public void checkFinder(final Finder finder) {
		Boolean result = true;

		if (finder.getMaxPrice() != null)
			if (finder.getMaxPrice() < 0)
				result = false;
		if (finder.getMinPrice() != null)
			if (finder.getMinPrice() < 0)
				result = false;

		Assert.isTrue(result);
	}

	// Get finder's results
	public Collection<Task> finderResults(final Finder f) {
		final Collection<Task> result = new ArrayList<Task>();

		if (f.getKeyWord() != null) {
			final Collection<Task> temp1 = this.finderRepository.findTasksByKeyWordTicker(f.getKeyWord());
			final Collection<Task> temp2 = this.finderRepository.findTasksByKeyWordDescription(f.getKeyWord());
			final Collection<Task> temp3 = this.finderRepository.findTasksByKeyWordAddress(f.getKeyWord());
			final Collection<Task> temp = new ArrayList<Task>();
			temp.addAll(temp1);
			temp.addAll(temp2);
			temp.addAll(temp3);

			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		if (f.getMaxPrice() != null) {
			final Collection<Task> temp = this.finderRepository.findTasksByMaxPrice(f.getMaxPrice());

			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		if (f.getMinPrice() != null) {
			final Collection<Task> temp = this.finderRepository.findTasksByMinPrice(f.getMinPrice());

			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		if (f.getStartDate() != null) {
			final Collection<Task> temp = this.finderRepository.findTasksByStartDate(f.getStartDate());
			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		if (f.getEndDate() != null) {
			final Collection<Task> temp = this.finderRepository.findTasksByEndDate(f.getEndDate());
			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		if (f.getCategory() != null) {
			final Collection<Task> temp = this.finderRepository.findTasksByCategory(f.getCategory());
			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		if (f.getWarranty() != null) {
			final Collection<Task> temp = this.finderRepository.findTasksByWarranty(f.getWarranty());
			if (temp.isEmpty()) {
				result.clear();
				return result;
			} else if (result.isEmpty())
				result.addAll(temp);
			else
				result.retainAll(temp);
		}

		return result;
	}

	public boolean finderUpdate(final Finder f) {
		boolean result = false;
		final Finder finder = this.finderRepository.findOne(f.getId());

		final Customization c = this.customizationService.findCustomization();
		final int finderCache = c.getFinderCache();

		final Date cacheMoment = DateUtils.addHours(finder.getMoment(), finderCache);

		final Date now = new Date();

		if (now.after(cacheMoment))
			result = true;

		return result;
	}
}
