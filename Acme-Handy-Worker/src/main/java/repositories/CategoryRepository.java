
package repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Category;
import domain.Task;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

	@Query("select t from Task t where t.category.id =?1")
	Task findTaskByCategory(int categoryId);

	@Query("select c from Category c where c.name = 'CATEGORY'")
	Category findCategory();

}
