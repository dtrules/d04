
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import domain.Application;

@Repository
public interface ApplicationRepository extends JpaRepository<Application, Integer> {

	@Query("select avg(a.price), min(a.price), max(a.price), stddev(a.price) from Application a")
	Collection<Double> findAvgMinMaxStddevByApplication();

	@Query("select count(a1)*1.0/(select count(a2) from Application a2) from Application a1" + " where a1.status = 'PENDING'")
	Double findRatioOfPendingApplications();

	@Query("select count(a1)*1.0/(select count(a2) from Application a2) from Application a1" + " where a1.status = 'ACCEPTED'")
	Double findRatioOfAcceptedApplications();

	@Query("select count(a1)*1.0/(select count(a2) from Application a2) from Application a1" + " where a1.status = 'REJECTED'")
	Double findRatioOfRejectedApplications();

	@Query("select count(a1)*1.0/(select count(a2) from Application a2) from Application a1" + " where a1.status = 'PENDING' AND a1.task.endMoment<CURRENT_TIME")
	Double findRatioOfPendingApplicationsElapsed();

	@Query("select a from Application a where a.task.id = ?1")
	Application getApplicationByTask(final int taskId);
}
