
package repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import security.UserAccount;
import domain.Actor;
import domain.Administrator;

@Repository
public interface AdministratorRepository extends JpaRepository<Administrator, Integer> {

	@Query("select a from Administrator a where a.userAccount.username = ?1")
	Administrator findAdministratorByUsername(String username);

	@Query("select a from Administrator a where a.userAccount = ?1")
	Administrator findAdministratorByUserAccount(UserAccount userAccount);

	@Query("select a from Actor a where a.isSuspicious=true")
	Collection<Actor> findSuspicious();

}
